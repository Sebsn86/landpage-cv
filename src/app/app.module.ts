import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { TransferHttpCacheModule } from "@nguniversal/common";

import { LayoutComponent } from "./layout/layout.component";

import { AppComponent } from "./app.component";
import { routes } from "./app.routes";

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent,
        LayoutComponent,
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: "my-app" }),

        RouterModule.forRoot(routes),
        TransferHttpCacheModule,
    ],
    providers: [],
})
export class AppModule {
}
