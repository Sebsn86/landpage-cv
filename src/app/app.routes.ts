import { Routes } from "@angular/router";

import { LayoutComponent } from "./layout/layout.component";

export const routes: Routes = [
    {
        children: [
            {
                loadChildren: "./layout/home/home.module#HomeModule",
                path: "",
            },
        ],
        component: LayoutComponent,
        path: "",
    },
    {
        path: "**",
        pathMatch: "full",
        redirectTo: "",
    },
];
